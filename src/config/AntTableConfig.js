const userColumns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'User ID',
    dataIndex: 'uid',
    key: 'uid',
  },
  {
    title: 'Cluster ID',
    dataIndex: 'clusterId',
    key: 'clusterId',
  },
  {
    title: 'User Role',
    dataIndex: 'userRole',
    key: 'userRole',
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Gender',
    dataIndex: 'gender',
    key: 'gender',
  },
  {
    title: 'Mobile',
    dataIndex: 'phoneNumber',
    key: 'phoneNumber',
  },
  {
    title: 'Email',
    dataIndex: 'email',
    key: 'email',
  },
];

const rideColumns = [
  {
    title: 'Ride ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Ride Status',
    dataIndex: 'status',
    key: 'status',
  },
  {
    title: 'Ride Type',
    dataIndex: 'type',
    key: 'type',
  },
  {
    title: 'Customer ID',
    dataIndex: 'customerId',
    key: 'customerId',
  },
  {
    title: 'Driver ID',
    dataIndex: 'driverId',
    key: 'driverId',
  },
  {
    title: 'Start Cluster ID',
    dataIndex: 'startClusterId',
    key: 'startClusterId',
  },
  {
    title: 'Start Location',
    dataIndex: 'startLocation',
    key: 'startLocation',
  },
  {
    title: 'End Cluster ID',
    dataIndex: 'endClusterId',
    key: 'endClusterId',
  },
  {
    title: 'End Location',
    dataIndex: 'endLocation',
    key: 'endLocation',
  },
];

export { userColumns, rideColumns };
