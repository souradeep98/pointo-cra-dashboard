const constants = {
  USER_CUSTOMER: 'user',
  USER_DRIVER: 'driver',
  SOCKET_RIDES: 'Socket Data - Rides',

  METRIC_1: 'Total Users',
  METRIC_2: 'Total Drivers',
  METRIC_3: 'Ongoing Rides',
  METRIC_4: 'Total Rides',
  METRIC_5: 'Drivers Earning',
  METRIC_6: 'Our Earning',

  DASHBOARD_LOGO: 'https://www.pointo.in/assets/img/Logo.webp',
  FOOTER_CONTENT: '©2021 Developed by Pointo'
};

export default constants;
