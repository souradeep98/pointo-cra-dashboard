import constants from '../config/constants';

const customerData = (userData) => {
  const customers = userData.filter(
    ({ userRole }) => userRole === constants.USER_CUSTOMER
  );

  return customers;
};

const driverData = (userData) => {
  const drivers = userData.filter(
    ({ userRole }) => userRole === constants.USER_DRIVER
  );

  return drivers;
};

const rides = (rideData) => {};

export { customerData, driverData, rides };
