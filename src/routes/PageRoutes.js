import React from 'react';
import { Route } from 'react-router-dom';
import { routes } from './Routes';

function RouteWithSubRoutes(route) {
  return (
    <Route
      exact
      path={route.path}
      render={(props) => (
        // pass the sub-routes down to keep nesting
        <route.component {...props} routes={route.routes} />
      )}
    />
  );
}

function PageRoutes() {
  return routes.map((route, i) => <RouteWithSubRoutes key={i} {...route} />);
}

export { PageRoutes, RouteWithSubRoutes };
