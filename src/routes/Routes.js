import Dashboard from '../pages/Dashboard';
import DriverDetails from '../pages/DriverDetails';
import RideDetails from '../pages/RideDetails';
import TestSocket from '../pages/TestSocket';
import UserDetails from '../pages/UserDetails';
import EmployeeProfile from '../pages/EmployeeProfile';
import NotFoundError from '../pages/ErrorPages/NotFoundError';

const routePaths = {
  HOME: '/',
  TEST_SOCKET: '/testSocket',
  REQUESTED_RIDES: '/book_rides/requested-rides',
  MAP_VIEW: '/book_rides/map-view',
  USERS: '/users',
  ACCOUNTING: '/accounting',
  BUSINESS_SETTINGS: '/business-settings',
  CUSTOMER_DETAILS: '/testSocket/customer/:id',
  DRIVER_DETAILS: '/testSocket/driver/:id',
  RIDE_DETAILS: '/testSocket/ride/:id',
  EMPLOYEE_PROFILE: '/employee/basic-settings',
  EMPLOYEE_SETTINGS: '/employee/account-settings',
  ASSIGN_USER_ROLE: '/employee/assign-user-role',
  ERROR_404: '/404',
};

const routes = [
  { path: routePaths.HOME, component: Dashboard },
  { path: routePaths.TEST_SOCKET, component: TestSocket },
  { path: routePaths.CUSTOMER_DETAILS, component: UserDetails },
  { path: routePaths.DRIVER_DETAILS, component: DriverDetails },
  { path: routePaths.RIDE_DETAILS, component: RideDetails },
  { path: routePaths.EMPLOYEE_PROFILE, component: EmployeeProfile },
  { path: routePaths.EMPLOYEE_SETTINGS, component: EmployeeProfile },
  { path: routePaths.ERROR_404, component: NotFoundError },
];

export { routePaths, routes };
