import React, { useState } from 'react';
import { Layout, Menu, Image } from 'antd';
import {
  ApiOutlined,
  DesktopOutlined,
  CarOutlined,
  HourglassOutlined,
  EnvironmentOutlined,
  TeamOutlined,
  BarChartOutlined,
  SettingOutlined,
} from '@ant-design/icons';
import { Content } from 'antd/lib/layout/layout';
import { Link } from 'react-router-dom';
import { routePaths } from '../routes/Routes';
import constants from '../config/constants';
import 'antd/dist/antd.css';
import './styles/styles.css';

const { Sider } = Layout;
const { SubMenu } = Menu;

function SideMenu() {
  const [keyState, setKeyState] = useState('1');

  return (
    <Sider
      breakpoint='lg'
      collapsedWidth='0'
      onBreakpoint={(broken) => {
        console.log(broken);
      }}
      onCollapse={(collapsed, type) => {
        console.log(collapsed, type);
      }}>
      <Content
        className='logo'
        style={{
          display: 'flex',
          justifyContent: 'center',
          padding: '0.8rem',
          marginTop: '0.4rem',
          marginBottom: '0.5rem',
        }}>
        <Link to={routePaths.HOME}>
          <Image
            width={100}
            height={40}
            src={constants.DASHBOARD_LOGO}
            preview={false}
          />
        </Link>
      </Content>

      <Menu
        selectedKeys={[keyState]}
        defaultOpenKeys={['sub1']}
        mode='inline'
        theme='dark'
        onSelect={({ key }) => {
          setKeyState(key);
          // console.log(key);
        }}
        //   inlineCollapsed={this.state.collapsed}
      >
        <Menu.Item key='1' icon={<DesktopOutlined />}>
          <Link to={routePaths.HOME}>Dashboard</Link>
        </Menu.Item>
        <Menu.Item key='2' icon={<ApiOutlined />}>
          <Link to={routePaths.TEST_SOCKET}>Test Socket</Link>
        </Menu.Item>
        <SubMenu key='sub1' icon={<CarOutlined />} title='Book Rides'>
          <Menu.Item key='3' icon={<HourglassOutlined />}>
            <Link to={routePaths.REQUESTED_RIDES}>Requested Rides</Link>
          </Menu.Item>
          <Menu.Item key='4' icon={<EnvironmentOutlined />}>
            <Link to={routePaths.MAP_VIEW}>Map View</Link>
          </Menu.Item>
        </SubMenu>
        <Menu.Item key='5' icon={<TeamOutlined />} title='Users'>
          <Link to={routePaths.USERS}>Users</Link>
        </Menu.Item>
        <Menu.Item key='6' icon={<BarChartOutlined />}>
          <Link to={routePaths.ACCOUNTING}>Accounting</Link>
        </Menu.Item>
        <Menu.Item key='7' icon={<SettingOutlined />}>
          <Link to={routePaths.BUSINESS_SETTINGS}>Business Settings</Link>
        </Menu.Item>
      </Menu>
    </Sider>
  );
}

export default SideMenu;
