import React, { useState } from 'react';
import { Table, Input } from 'antd';

const { Search } = Input;

function AntTable({ columns, data, searchPlaceholder }) {
  const [dataSource, setDataSource] = useState(data);
  // const [search, setSearch] = useState('');

  function formatData(item) {
    return Object.values(item).map((field) => {
      let val;
      if (field !== null) {
        val = field.toString().toLowerCase();
      }
      return val;
    });
  }

  function onChange(e) {
    const search = e.target.value;
    // setSearch(currValue);

    if (search === '') {
      setDataSource(data);
    } else {
      let filteredData = data.filter((item) =>
        formatData(item).includes(search.toLowerCase())
      );
      setDataSource(filteredData);
    }
  }

  return (
    <>
      <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
        <Search
          placeholder={searchPlaceholder}
          allowClear
          onChange={onChange}
          style={{ width: 200, marginBottom: '1rem' }}
        />
      </div>
      <Table
        columns={columns}
        dataSource={dataSource}
        bordered={true}
        size={'small'}
      />
    </>
  );
}

export default AntTable;
