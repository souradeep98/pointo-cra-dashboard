import React from 'react';
import { Layout } from 'antd';
import constants from '../../config/constants';

const { Footer } = Layout;

function AntFooter() {
  return (
    <Footer style={{ textAlign: 'center' }}>{constants.FOOTER_CONTENT}</Footer>
  );
}

export default AntFooter;
