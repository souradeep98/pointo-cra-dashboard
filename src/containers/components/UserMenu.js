import React from 'react';
import { Menu, Dropdown, Button, Space, Avatar, Divider } from 'antd';
import {
  UserOutlined,
  SettingOutlined,
  LogoutOutlined,
  ExclamationCircleOutlined,
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { routePaths } from '../../routes/Routes';
import './styles/style.css';

function UserMenu({ userName, userRole }) {
  const displayName = userName || 'User';
  const role = userRole || 'Admin';

  const menu = (
    <Menu>
      <p style={{ marginLeft: '0.7rem' }}>
        <span>
          <ExclamationCircleOutlined style={{ marginRight: '0.5rem' }} />
        </span>
        {role}
      </p>

      <Divider style={{ marginBottom: '0.2rem', marginTop: '-0.6rem' }} />
      <Menu.Item key='1'>
        <UserOutlined style={{ marginRight: '0.5rem' }} />
        <Link to={routePaths.EMPLOYEE_PROFILE}>Profile</Link>
      </Menu.Item>
      <Menu.Item key='2'>
        <SettingOutlined style={{ marginRight: '0.5rem' }} />
        <Link to={routePaths.EMPLOYEE_SETTINGS}>Settings</Link>
      </Menu.Item>
      <Divider style={{ marginBottom: '0.2rem', marginTop: '0.3rem' }} />
      <Menu.Item key='3'>
        <LogoutOutlined style={{ marginRight: '0.5rem' }} />
        <Link to=''>Logout</Link>
      </Menu.Item>
    </Menu>
  );

  return (
    <Space direction='vertical' style={{ marginRight: '1rem' }}>
      <Space wrap>
        <Dropdown overlay={menu} placement='bottomCenter'>
          <Button className='profile-button'>
            <Avatar className='avatar' size={'small'}>
              {displayName.charAt(0).toUpperCase()}
            </Avatar>
            {displayName}
          </Button>
        </Dropdown>
      </Space>
    </Space>
  );
}

export default UserMenu;
