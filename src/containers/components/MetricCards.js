import React from 'react';
import { Card, Statistic, Col, Row } from 'antd';

function MetricCard({
  title1,
  title2,
  title3,
  title4,
  title5,
  title6,
  data1,
  data2,
  data3,
  data4,
  data5,
  data6,
}) {
  return (
    <div className='site-card-wrapper'>
      <Row gutter={16}>
        <Col span={4}>
          <Card bordered={false}>
            <Statistic title={title1} value={data1} />
          </Card>
        </Col>
        <Col span={4}>
          <Card bordered={false}>
            <Statistic title={title2} value={data2} />
          </Card>
        </Col>
        <Col span={4}>
          <Card bordered={false}>
            <Statistic title={title3} value={data3} />
          </Card>
        </Col>
        <Col span={4}>
          <Card bordered={false}>
            <Statistic title={title4} value={data4} />
          </Card>
        </Col>
        <Col span={4}>
          <Card bordered={false}>
            <Statistic title={title5} value={data5} />
          </Card>
        </Col>
        <Col span={4}>
          <Card bordered={false}>
            <Statistic title={title6} value={data6} />
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default MetricCard;
