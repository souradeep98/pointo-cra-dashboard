import React from 'react';
import { Layout } from 'antd';
import 'antd/dist/antd.css';
import './styles/styles.css';
import UserMenu from './components/UserMenu';

const { Header } = Layout;

function NavBar() {
  return (
    <Header className='site-layout-sub-header-background'>
      <UserMenu />
    </Header>
  );
}

export default NavBar;
