import React from 'react';
import { Layout } from 'antd';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import SideMenu from './containers/SideBar';
import NavBar from './containers/NavBar';
import { PageRoutes } from './routes/PageRoutes';
import AntFooter from './containers/components/AntFooter';
import 'antd/dist/antd.css';

function App() {
  return (
    <Router>
      <Layout style={{ minHeight: '100vh' }}>
        <SideMenu />
        <Layout>
          <NavBar />
          <Switch>
            <PageRoutes />
          </Switch>
          <AntFooter />
        </Layout>
      </Layout>
    </Router>
  );
}

export default App;
