import React from 'react';
import { Layout, Typography } from 'antd';
import MetricCard from '../containers/components/MetricCards';
import constants from '../config/constants';

const { Content } = Layout;
const { Title } = Typography;

function Dashboard() {
  return (
    <Content style={{ margin: '24px 16px 16px 16px' }}>
      <Title level={3} style={{ marginBottom: '1.5rem' }}>
        Dashboard
      </Title>
      <MetricCard
        title1={constants.METRIC_1}
        title2={constants.METRIC_2}
        title3={constants.METRIC_3}
        title4={constants.METRIC_4}
        title5={constants.METRIC_5}
        title6={constants.METRIC_6}
        data1={2500}
        data2={500}
        data3={5646}
        data4={51565}
        data5={54925}
        data6={545951}
      />
    </Content>
  );
}

export default Dashboard;
