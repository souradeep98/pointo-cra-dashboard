import React from 'react';
import { Layout, Result, Button } from 'antd';
import { Link } from 'react-router-dom';
import { routePaths } from '../../routes/Routes';

const { Content } = Layout;

function NotFoundError() {
  return (
    <Content style={{ margin: '24px 16px 16px 16px' }}>
      <Result
        status='404'
        title='404'
        subTitle='Sorry, the page you visited does not exist.'
        extra={
          <Button type='primary'>
            <Link to={routePaths.HOME}>Back Home</Link>
          </Button>
        }
      />
    </Content>
  );
}

export default NotFoundError;
