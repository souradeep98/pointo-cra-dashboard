import React from 'react';
import { Layout, Result, Button } from 'antd';
import { Link } from 'react-router-dom';
import { routePaths } from '../../routes/Routes';

const { Content } = Layout;

function ServerError() {
  return (
    <Content style={{ margin: '24px 16px 16px 16px' }}>
      <Result
        status='500'
        title='500'
        subTitle='Sorry, something went wrong.'
        extra={
          <Button type='primary'>
            <Link to={routePaths.HOME}>Back Home</Link>
          </Button>
        }
      />
    </Content>
  );
}

export default ServerError;
