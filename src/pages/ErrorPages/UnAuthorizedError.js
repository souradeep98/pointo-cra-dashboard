import React from 'react';
import { Layout, Result, Button } from 'antd';
import { Link } from 'react-router-dom';
import { routePaths } from '../../routes/Routes';

const { Content } = Layout;

function UnAuthorizedError() {
  return (
    <Content style={{ margin: '24px 16px 16px 16px' }}>
      <Result
        status='403'
        title='403'
        subTitle='Sorry, you are not authorized to access this page.'
        extra={
          <Button type='primary'>
            <Link to={routePaths.HOME}>Back Home</Link>
          </Button>
        }
      />
    </Content>
  );
}

export default UnAuthorizedError;
