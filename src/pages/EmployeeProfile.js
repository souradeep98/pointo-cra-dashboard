import React from 'react';
import { Layout, Card, Menu } from 'antd';
import {
  UserOutlined,
  SettingOutlined,
  UserAddOutlined,
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { routePaths } from '../routes/Routes';

const { Content } = Layout;

function EmployeeProfile() {
  return (
    <Content style={{ margin: '24px 16px 16px 16px' }}>
      <Card>
        <Menu
          style={{ width: 256 }}
          defaultSelectedKeys={['1']}
          defaultOpenKeys={['sub1']}
          mode={'inline'}>
          <Menu.Item key='1' icon={<UserOutlined />}>
            <Link to={routePaths.EMPLOYEE_PROFILE}> Basic Profile </Link>
          </Menu.Item>
          <Menu.Item key='2' icon={<SettingOutlined />}>
            <Link to={routePaths.EMPLOYEE_SETTINGS}> Account Settings </Link>
          </Menu.Item>
          <Menu.Item key='3' icon={<UserAddOutlined />}>
          <Link to={routePaths.ASSIGN_USER_ROLE}>Assign Employee Roles </Link>
          </Menu.Item>
        </Menu>
      </Card>
    </Content>
  );
}

export default EmployeeProfile;
