import { Layout, Typography } from 'antd';
import React from 'react';

const { Content } = Layout;
const { Title } = Typography;

function UserDetails() {
  return (
    <Content style={{ margin: '24px 16px 16px 16px' }}>
      <Title level={3} style={{ marginBottom: '1.5rem' }}>
        User Details
      </Title>
    </Content>
  );
}

export default UserDetails;
