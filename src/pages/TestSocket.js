import React from 'react';
import { Layout, Tabs, Card, Typography } from 'antd';
import AntTable from '../containers/components/AntTable';
import { rideColumns, userColumns } from '../config/AntTableConfig';
import { userData, rideData } from '../services/mockData';
import { customerData, driverData } from '../utils/filterSocketData';
// import { io } from 'socket.io-client';

// const socket = io('https://api-dev.pointo.in');

const { Content } = Layout;
const { TabPane } = Tabs;
const { Title } = Typography;

function TestSocket() {
  return (
    <Content style={{ margin: '24px 16px 16px 16px' }}>
      <Title level={3} style={{ marginBottom: '1.5rem' }}>
        Test Socket
      </Title>
      <Card>
        <Tabs defaultActiveKey='1'>
          <TabPane tab='User' key='1'>
            <AntTable
              columns={userColumns}
              data={customerData(userData)}
              searchPlaceholder={'Search users'}
            />
          </TabPane>
          <TabPane tab='Driver' key='2'>
            <AntTable
              columns={userColumns}
              data={driverData(userData)}
              searchPlaceholder={'Search drivers'}
            />
          </TabPane>
          <TabPane tab='Rides' key='3'>
            <AntTable
              columns={rideColumns}
              data={rideData}
              searchPlaceholder={'Search rides'}
            />
          </TabPane>
        </Tabs>
      </Card>
    </Content>
  );
}

export default TestSocket;
